	@Servicio(codeCircuitBreaker = 1105, projectName = "TiempoAire")
	public ResponseEntity<Object> getOperadoras() {
		LOG.info("Entra al metodo de getOperadoras :::: TiempoAireComponent");

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response;

		HttpEntity<String> httpEntity = new HttpEntity<String>(null, getHeaders(Constantes.ARCUS_SANDBOX_BILLER));
		try {
			response = restTemplate.postForEntity(Constantes.ARCUS_SANDBOX_BASE_URL + Constantes.ARCUS_SANDBOX_BILLER,
					httpEntity, String.class);

			// Convert Json response to java Object
			JSONObject jsonResponse = new JSONObject(response.getBody());
			LOG.info("jsonResponse... {}", jsonResponse);
			
			JSONArray operadorasJson = jsonResponse.getJSONArray("billers");

			LOG.info("operadorasJson... {}", operadorasJson);

         // agrega lista from json object into java class
			List<OperadorasBillResponse> ListaOperadoras = new Gson().fromJson(operadorasJson.toString(),
					new TypeToken<ArrayList<OperadorasBillResponse>>() {
					}.getType());

			OperadorasBillResultado resultado = new OperadorasBillResultado();

			resultado.setOperadoras(ListaOperadoras);

			OperadorasBillResponseTO responseTO = new OperadorasBillResponseTO();
			responseTO.setResultado(resultado);
			return new ResponseEntity<>(responseTO, HttpStatus.OK);

		} catch (HttpClientErrorException e) {
			LOG.info("Incidencia" + e.getMessage() + e.getResponseBodyAsString());
			throw new MessageException(1101, HttpStatus.BAD_REQUEST);
		}
	}
	
	
	
    @Servicio(codeCircuitBreaker = 1105, projectName = "TiempoAire")
	public ResponseEntity<Object> pagarRecarga(PagoRequest request, String aplicacion) {
		LOG.info("Entra al metodo de pagarRecarga :::: TiempoAireComponent");

		LOG.info("PagoRequest... {}", request);

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response;

		HttpEntity<Object> httpEntity = new HttpEntity<>(request, getHeaders(Constantes.ARCUS_SANDBOX_PAYBILl));
		try {
			response = restTemplate.postForEntity(Constantes.ARCUS_SANDBOX_BASE_URL + Constantes.ARCUS_SANDBOX_PAYBILl,
					httpEntity, String.class);

			LOG.info("Response... {}", response.getBody());
            // Convert Json response to java Object
            JSONObject jsonResponse = new JSONObject(response.getBody());
            
            
			PagoResponseTO responseTO = new PagoResponseTO();
			// Convert json Response to Java Class
			PagoTAResponse resultado = new Gson().fromJson(jsonResponse.toString(), PagoTAResponse.class);
			responseTO.setResultado(resultado);

			return new ResponseEntity<>(responseTO, HttpStatus.OK);
		} catch (HttpClientErrorException e) {
			LOG.info("Incidencia" + e.getMessage() + e.getResponseBodyAsString());
			throw new MessageException(1101, HttpStatus.BAD_REQUEST);
		}
	}
	
	
	// =================================================  ENTITIES ====================================
@ApiModel("OperadorasResponseTO")
public class OperadorasBillResponseTO extends ResponseTO {

	private OperadorasBillResultado resultado;

	public OperadorasBillResultado getResultado() {
		return resultado;
	}

	public void setResultado(OperadorasBillResultado resultado) {
		this.resultado = resultado;
	}

}


@ApiModel("OperadorasResultado")
public class OperadorasBillResultado {

	private List<OperadorasBillResponse> operadoras;

	public List<OperadorasBillResponse> getOperadoras() {
		return operadoras;
	}

	public void setOperadoras(List<OperadorasBillResponse> operadoras) {
		this.operadoras = operadoras;
	}

}

@ApiModel("OperadorasResponse")
public class OperadorasBillResponse {

	private String id;
	private String name;
	private String country;
	private String currency;
	private String biller_type;
	private String bill_type;
	private String can_check_balance;
	private String mask;
	private String requires_name_on_account;
	private String supports_partial_payments;
	private String hours_to_fulfill;
	private String account_number_digits;
	private String has_xdata;
	
	// getters and setters
}

// ========================== Response Json =======================
{
   "billers":[
      {
         "country":"US",
         "hours_to_fulfill":72,
         "can_check_balance":false,
         "supports_partial_payments":true,
         "account_number_digits":"7..15",
         "has_xdata":false,
         "requires_name_on_account":false,
         "name":"(Ahs) American Home Shield",
         "bill_type":"account_number",
         "currency":"USD",
         "id":8925,
         "biller_type":"Utility",
         "mask":null
      },
      {
         "country":"US",
         "hours_to_fulfill":72,
         "can_check_balance":false,
         "supports_partial_payments":true,
         "account_number_digits":"16",
         "has_xdata":false,
         "requires_name_on_account":false,
         "name":"1 800 Flowers",
         "bill_type":"account_number",
         "currency":"USD",
         "id":10938,
         "biller_type":"Utility",
         "mask":null
      }
   ]
}
