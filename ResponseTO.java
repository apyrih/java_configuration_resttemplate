package com.java.api.commons.utils.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Request base del cual deben extender todos los response que se construyan en la aplicacion, tiene 
 * campos necesarios en todas las respuestas que se hagan a los servicios
 *
 */
@ApiModel("ResponseTO")
public class ResponseTO {
	
	@ApiModelProperty(value = "codigo", notes = "Codigo de la operacion",example="0")
	private String codigo;
	@ApiModelProperty(value = "mensaje", notes = "Mensaje de la operacion",example="Operacion exitosa")
	private String mensaje;
	@ApiModelProperty(value = "folio", notes = "Folio de la operacion",example="15720180927175536771")
	private String folio;
	@ApiModelProperty(notes = "advertencia",example="Se recomienda el uso adecuado de la aplicaci\u00f3n, cualquier uso indebido ser\u00e1 sancionado")
	private String advertencia = "Se recomienda el uso adecuado de la aplicaci\u00f3n, cualquier uso indebido ser\u00e1 sancionado";
	
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigoOperacion) {
		this.codigo = codigoOperacion;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String descripcion) {
		this.mensaje = descripcion;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	@Override
	public String toString() {
		return "{codigo=" + codigo + ", mensaje=" + mensaje + ", folio=" + folio + "}";
	}
	public String getAdvertencia() {
		return advertencia;
	}
	public void setAdvertencia(String advertencia) {
		this.advertencia = advertencia;
	}
}
