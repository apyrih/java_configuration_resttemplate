package com.java.api.component;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class APIComponent {

     // Initialize LOG and Date variables format
     private static final Logger LOG = LoggerFactory.getLogger(APIComponent.class);
     private static final String FORMAT = "yyyy-MM-dd";
     private static final String DATE_TIME_REQUEST_FORMAT = "yyyy-MM-dd HH:mm:ss.ssss";

	// Read the properties values
	PropertiesManager properties = PropertiesManager.getInstance();
	private String property_file = Constantes.PROPERTIES_FILE;
	
	@Servicio(codeCircuitBreaker = 1105, projectName = "APIs")
	public ResponseEntity<Object> APIService() {
		LOG.info("Entra al metodo de APIService :::: APIComponent");

		Configuration configuration = new Configuration(Constantes.ARCUS_SANDBOX, ARCUS_APIAuth, ARCUS_SecretKey);
		String fecha = configuration.getDate();
		String endpoint = "/account";
		final String authHash = new AuthHash(configuration).generate(null, endpoint, fecha);

		StringBuilder checksumString = new StringBuilder();
		checksumString.append(configuration.getContentType() + ",");
		checksumString.append(",");
		checksumString.append(endpoint + ",");
		checksumString.append(fecha);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", configuration.getAccept());
		headers.add("Content-Type", configuration.getContentType());
		headers.add("Date", fecha);
		headers.add("Authorization", authHash);
		headers.add("Content-MD5", "");

		LOG.info("Url Service... {}", Constantes.ARCUS_SANDBOX);
		LOG.info("Request... {}", checksumString.toString());
		LOG.info("Header... {}", headers);

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response;

		HttpEntity<String> httpEntity = new HttpEntity<String>(null, headers);

		LOG.info("httpEntity... {}", httpEntity);

		try {
			response = restTemplate.postForEntity(Constantes.ARCUS_SANDBOX, httpEntity, String.class);
			LOG.info("response... {}", response.getBody());
            
            // Convert response.getBody() to a JSON Object
			JSONObject jsonResponse = new JSONObject(response.getBody());

			ApiResponseTO responseTO = new ApiResponseTO();

			Resultado resultado = new Resultado();

			LOG.info("jsonResponse name => " + jsonResponse.get("name").toString());

			resultado.setName(jsonResponse.get("name").toString());
			resultado.setBalance(jsonResponse.get("balance").toString());
			resultado.setMinimum_balance(jsonResponse.get("minimum_balance").toString());
			resultado.setCurrency(jsonResponse.get("currency").toString());

			responseTO.setResultado(resultado);
			return new ResponseEntity<>(responseTO, HttpStatus.OK);

		} catch (HTTPException e) {
			LOG.info("Incidencia" + e.getMessage());
			throw new MessageException(1101, HttpStatus.BAD_REQUEST);
		}

	}

}